import pandas as pd
from sklearn.svm import SVC

# Cargamos los datos
data = pd.read_csv("data.csv", sep=";")

# Convertir las columnas de síntomas en variables binarias
data_encoded = pd.get_dummies(data.iloc[:, :-1])

# Obtener la columna de enfermedad
enfermedad = data["enfermedad"]

# Entrenamos el modelo
model = SVC()
model.fit(data_encoded, enfermedad)

# Preguntar al paciente por los síntomas
respuestas = []
for columna in data_encoded.columns:
  respuesta = input(f"¿Presenta el síntoma {columna}? (si/no): ")
  if respuesta.lower() == "si":
    respuestas.append(1)
  else:
    respuestas.append(0)

# Crear un DataFrame con las respuestas del paciente
data_paciente = pd.DataFrame([respuestas], columns=data_encoded.columns)

# Realizar la predicción
enfermedad_pred = model.predict(data_paciente)

# Obtener el resultado
resultado = enfermedad_pred[0]
print("El paciente podría tener la enfermedad:", resultado)
